# Clear the console on exit for privacy, when at the top level shell (SHLVL=1)
if [ "$SHLVL" -eq 1 ]; then
    if command -v clear &>/dev/null; then
        clear
    fi
fi

# Make sure history is written upon logout
history -a
