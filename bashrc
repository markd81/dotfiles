#
# https://gitlab.com/markd81/dotfiles
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

BASHCONFIG="$HOME/.config/bash"
# Set history file location and size
export HISTFILE=$BASHCONFIG/history
export HISTSIZE=5000000
export HISTFILESIZE=$HISTSIZE
# History options
export HISTCONTROL=ignoredups:erasedups
export HISTIGNORE="&:[ ]*:exit:ls:bg:fg:history:clear:cd"
export HISTTIMEFORMAT="%Y-%m-%d %H:%M:%S "
# dynamically update history in sessions
shopt -s histappend
export PROMPT_COMMAND="history -a; history -n"

# make sure user bin directory is added to PATH
if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ] ; then
  PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "$HOME/local/bin" ] ; then
  PATH="$HOME/local/bin:$PATH"
fi

# Source additional settings if they exist
for file in "$BASHCONFIG/"{aliases,functions,environment}; do
    [[ -f "$file" ]] && source "$file"
done

# Define colors for the prompt
RESET="\[\e[0m\]"
GREEN="\[\e[32m\]"
BLUE="\[\e[34m\]"
CYAN="\[\e[36m\]"
BOLD_YELLOW="\[\e[1;33m\]"

# Set up a colorful prompt with dynamic Git branch
PS1="${BOLD_YELLOW}[\u@\h ${BLUE}\W${CYAN} \$(parse_git_branch)${BOLD_YELLOW}]\$ ${RESET}"

# show some system information like nerdfetch did once
sysinfo
setup_ssh_agent "$SSHKEYS"
