# if running bash, include .bashrc if it exists
if [ -n "$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
# only when no .bashrc exists
if [ ! -f "$HOME/.bashrc" ]; then
  if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
  fi
  if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
  fi
  if [ -d "$HOME/local/bin" ] ; then
    PATH="$HOME/local/bin:$PATH"
  fi
fi

# Set the default editor if it's not set already
: ${EDITOR:=vi}
