#!/bin/bash

# run from the current directory
SOURCE_DIR="."
DEST_DIR="$HOME/.config"
DEST_HOME="$HOME"
FILES_TO_COPY="tmux.conf profile bashrc bash_logout Xresources"

# Copy all regular files (not directories) from the current directory to $HOME and rename them
for file in $FILES_TO_COPY; do
  if [ -f "$file" ]; then
    cp -f "$file" "$DEST_HOME/.${file##*/}"
  fi
done

# Ensure the destination directory exist
mkdir -p "$DEST_DIR"

cp -r "$SOURCE_DIR/bash" "$DEST_DIR/"
cp -r "$SOURCE_DIR/i3" "$DEST_DIR/"
